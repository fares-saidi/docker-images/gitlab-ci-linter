# Gitlab CI Linter

Docker image to be used in the Gitlab CI pipelines to lint Gitlab CI files using the CI Lint api

## Usage

```txt
usage: gitlab-ci-lint [-h] [-f FILE] [-d DIR] [-nr] [--api-url API_URL]
                      [--api-token API_TOKEN]
                      [--api-project-id API_PROJECT_ID]
                      [--show-merged-yaml]

CLI tool to validate Gitlab CI yaml definitions.
If no file or directory inputs are given, all .yml files in the working directory will be validated.

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  Gitlab .yml input file to validate
  -d DIR, --dir DIR     Directory containing Gitlab .yml input files to validate.
  -nr, --no-recursive   False by default. Tells the linting tool to gather all files under the listed directories only. If this flag is not set, all yml files will be gathered recursively.
  --api-url API_URL     Gitlab CI API URL that the linter will use. Defaults to "$CI_API_V4_URL" if available otherwise "https://gitlab.com/api/v4"
  --api-token API_TOKEN
                        Gitlab Access Token that the linter will use for the given API. Defaults to "$CI_JOB_TOKEN" if available otherwise is blank.
                        This will keep getting error 401 if a value is not set because the current scope of the default $CI_JOB_TOKEN does not allow `api` permissions.
                        ****Until at least api read permissions are added to default $CI_JOB_TOKEN, this value is required for the script to work****
                        CI_JOB_TOKEN will eventually work when permissions are set as planned.
                        Reference issues [ https://gitlab.com/groups/gitlab-org/-/epics/3559 - https://gitlab.com/gitlab-org/gitlab/-/issues/223678 ]
  --api-project-id API_PROJECT_ID
                        Gitlab Project ID to use for the validation of the yaml files.
                        If you have any local includes in your pipeline definitions, you will need this value to exist otherwise you will get a `Local file `xyz` does not have project!`
                        Defaults to $CI_PROJECT_ID so that you do not need to explicitely define it in your pipeline
  --ref REF             Branch or tag to use to validate against.
                        If you have any local includes in your pipeline definitions, you will need this value to exist otherwise your files will be validated against the default branch, where your changes to includes end up not being imported.
                        Defaults to $CI_COMMIT_REF_NAME so that you do not need to explicitely define it in your pipeline
  --show-merged-yaml    Prints the merged yaml when displaying the validation results. Used if the linted file has any includes
```

## Using the CI Linter image in your pipeline

Create an personal or group (within your project group) access token with `api` permissions, then save it in your repository's CI/CD variables with a desired key. In this case I'm using `LINT_TOKEN`.

```yaml
image: registry.gitlab.com/fares-saidi/docker-images/gitlab-ci-linter/gitlab-ci-lint:latest

validate-all-yaml-files-in-repository:
  stage: lint
  script:
    - gitlab-ci-lint --api-token=$LINT_TOKEN

validate-all-yaml-files-in-single-directory-non-recursively:
  stage: lint
  script:
    - gitlab-ci-lint --api-token=$LINT_TOKEN --dir example-pipelines --no-recursive

validate-single-file-in-repository:
  stage: lint
  script:
    - gitlab-ci-lint --api-token=$LINT_TOKEN --file .gitlab-ci.yml
```
