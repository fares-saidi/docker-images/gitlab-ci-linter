FROM python:3.6
LABEL Author="Fares Saidi"

WORKDIR /usr/local/bin

RUN apt-get update && apt-get install -y dos2unix

COPY requirements.txt /usr/local/bin/
RUN pip install --no-cache-dir -r requirements.txt

COPY gitlab-ci-lint /usr/local/bin
RUN dos2unix gitlab-ci-lint
RUN chmod +x gitlab-ci-lint

CMD gitlab-ci-lint
